package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class ScanReport {

    @JsonProperty("RESULTS")
    Results result;

    @JsonProperty("GLOSSARY")
    Glossary glossary;

    @JsonProperty("APPENDIX")
    Appendix appendix;
}
