package io.secodjo.ssc.parser.qualys;

public enum CustomVulnAttribute implements com.fortify.plugin.spi.VulnerabilityAttribute {
    
    url(AttrType.STRING),
    authentication(AttrType.STRING),
    request(AttrType.LONG_STRING),
    response(AttrType.LONG_STRING),
    category(AttrType.STRING),
    description(AttrType.LONG_STRING),
    impact(AttrType.LONG_STRING),
    solution(AttrType.LONG_STRING),
    group(AttrType.STRING),
    type(AttrType.STRING),
    cwe(AttrType.STRING),
    cvssBase(AttrType.STRING),
    cvssTemporal(AttrType.STRING),
    owasp(AttrType.STRING),
    wasc(AttrType.STRING),
    payload(AttrType.LONG_STRING),
	;

    private final AttrType attributeType;

    CustomVulnAttribute(final AttrType attributeType) {
        this.attributeType = attributeType;
    }

    @Override
    public String attributeName() {
        return name();
    }

    @Override
    public AttrType attributeType() {
        return attributeType;
    }
}
