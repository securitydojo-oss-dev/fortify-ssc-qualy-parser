package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Group {
    @JsonProperty("CODE")
    String code;
    @JsonProperty("NAME")
    String name;
    @JsonProperty("TYPE")
    String type;
}
