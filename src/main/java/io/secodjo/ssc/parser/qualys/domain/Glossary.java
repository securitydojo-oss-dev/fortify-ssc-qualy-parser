package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

@Getter
public class Glossary {
    @JacksonXmlElementWrapper(localName = "QID_LIST")
    @JacksonXmlProperty(localName = "QID")
    @JsonProperty QID[] qidList;

    @JacksonXmlElementWrapper(localName = "GROUP_LIST")
    @JacksonXmlProperty(localName = "GROUP")
    @JsonProperty Group[] groups;

    @JacksonXmlElementWrapper(localName = "OWASP_LIST")
    @JacksonXmlProperty(localName = "OWASP")
    @JsonProperty OWASP[] owaspList;

    @JacksonXmlElementWrapper(localName = "WASC_LIST")
    @JacksonXmlProperty(localName = "WASC")
    @JsonProperty WASC[] wascList;
}
