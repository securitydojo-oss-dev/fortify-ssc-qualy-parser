package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class WASC {
    @JsonProperty("CODE")
    String code;
    @JsonProperty("NAME")
    String name;

    public String getWasc(){
        return code + " : " + name;
    }
}
