package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

@Getter
public class Request {

    @JsonProperty("METHOD")
    String method;
    @JsonProperty("URL")
    String url;

    @JacksonXmlElementWrapper(localName = "HEADERS")
    @JacksonXmlProperty(localName = "HEADER")
    @JsonProperty Header[] headers;
}
