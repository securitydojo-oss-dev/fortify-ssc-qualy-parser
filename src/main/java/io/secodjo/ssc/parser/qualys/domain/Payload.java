package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Payload {

    @JsonProperty("PAYLOAD")
    String payload;
    @JsonProperty("REQUEST")
    Request request;
    @JsonProperty("RESPONSE")
    Response response;

}
