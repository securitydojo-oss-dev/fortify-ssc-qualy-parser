package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

@Getter
public class SeverityCategory {

    @JsonProperty("NAME")
    String name;
    @JsonProperty("DESCRIPTION")
    String description;

    @JacksonXmlElementWrapper(localName = "SEVERITY_LEVEL_LIST")
    @JacksonXmlProperty(localName = "SEVERITY_LEVEL")
    SeverityLevel[] severityLevels;
}
