package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class OWASP {
    @JsonProperty("CODE")
    String code;
    @JsonProperty("NAME")
    String name;

    public String getOwasp(){
        return code + " : " + name;
    }
}
