package io.secodjo.ssc.parser.qualys.parser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fortify.plugin.api.BasicVulnerabilityBuilder.Priority;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;
import io.secodjo.ssc.parser.qualys.CustomVulnAttribute;
import com.fortify.util.ssc.parser.EngineTypeHelper;
import com.fortify.util.ssc.parser.xml.ScanDataStreamingXmlParser;

import io.secodjo.ssc.parser.qualys.domain.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VulnerabilitiesParser {
	private static final Logger LOG = LoggerFactory.getLogger(VulnerabilitiesParser.class);
	private static final String ENGINE_TYPE = EngineTypeHelper.getEngineType();

	private static final Map<String, Priority> MAP_SEVERITY_TO_PRIORITY = Stream.of(
			  new AbstractMap.SimpleImmutableEntry<>("1", Priority.Low),
			  new AbstractMap.SimpleImmutableEntry<>("2", Priority.Medium),
			  new AbstractMap.SimpleImmutableEntry<>("3",Priority.High),
              new AbstractMap.SimpleImmutableEntry<>("4",Priority.Critical),
			  new AbstractMap.SimpleImmutableEntry<>("5",Priority.Critical))
		.collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));

	private final ScanData scanData;
	private final VulnerabilityHandler vulnerabilityHandler;

    public VulnerabilitiesParser(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) {
    	this.scanData = scanData;
		this.vulnerabilityHandler = vulnerabilityHandler;
	}
    
    /**
	 * Main method to commence parsing the input provided by the configured {@link ScanData}.
	 * @throws ScanParsingException
	 * @throws IOException
	 */

	public final void parse() throws ScanParsingException, IOException {
		new ScanDataStreamingXmlParser()
			.handler("/WAS_SCAN_REPORT", ScanReport.class, this::getVulnerabilities)
			.parse(scanData);
	}

	public final void getVulnerabilities(ScanReport scanReport){
		Vulnerability[] vulnerabilities = scanReport.getResult().getVulnerability();
		LOG.info("Nb vulnerabilities : " + vulnerabilities.length);
		for (Vulnerability vulnerability: vulnerabilities ) {
			this.buildVulnerabilities(vulnerability, scanReport);
		}
	}
	
	
	private void buildVulnerabilities(Vulnerability vulnerability, ScanReport scanReport) {
		StaticVulnerabilityBuilder vb = vulnerabilityHandler.startStaticVulnerability(vulnerability.getUniqueId());

		vb.setEngineType(ENGINE_TYPE);
		vb.setAnalyzer("Qualys");

		// Set mandatory values to JavaDoc-recommended values
		vb.setAccuracy(5.0f);
		vb.setLikelihood(2.5f);

		vb.setFileName(getFileName(vulnerability.getUrl()));
		vb.setStringCustomAttributeValue(CustomVulnAttribute.url, vulnerability.getUrl());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.authentication, vulnerability.getAuthentication());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.payload, this.formatPayloads(vulnerability.getPayloads()));

		QID qid = this.getVulnerabilityQID(vulnerability, scanReport);
		vb.setCategory(qid.getTitle());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.description, qid.getDescription());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.impact, qid.getImpact());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.solution, qid.getSolution());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.category, qid.getCategory());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.cwe, qid.getCwe());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.cvssBase, qid.getCvssBase());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.cvssTemporal, qid.getCvssTemporal());

		SeverityCategory severityCategory = this.getSeverityCategory(qid, scanReport);
		SeverityLevel severityLevel = this.getSeverityLevel(qid, severityCategory);
		vb.setPriority(getPriority(severityLevel));
		vb.setImpact(Float.parseFloat(severityLevel.getSeverity()));

		Group group = this.getGroup(qid, scanReport);
		vb.setStringCustomAttributeValue(CustomVulnAttribute.group, group.getName());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.type, group.getType());

		OWASP owasp = this.getOwasp(qid,scanReport);
		if (owasp != null) {
			vb.setStringCustomAttributeValue(CustomVulnAttribute.owasp, owasp.getOwasp());
		}

		WASC wasc = this.getWasc(qid,scanReport);
		if (wasc != null) {
			vb.setStringCustomAttributeValue(CustomVulnAttribute.wasc, wasc.getWasc());
		}

		vb.completeVulnerability();
    }

	private String getFileName(String url) {
		String result = this.getPath(url).replace("/",">");
		char firstChar = result.charAt(0);
		if(firstChar == '>'){
			result = result.substring(1);
		}
		char lastChar = result.charAt(result.length()-1);
		if(lastChar == '>'){
			result = result.substring(0,result.length()-1);
		}
		char[] ch = result.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (int i = 0 ; i < ch.length; i++){
			sb.append(ch[i]);
			if(ch[i] == '>' && ch[i+1] == '>'){
				while (ch[i+1] == '>'){
					i++;
				}
			}
		}
		return  sb.toString();
	}

	private String formatPayloads(Payload[] payloads) {
		StringBuilder sb = new StringBuilder();
		for (Payload payload:payloads ) {
			if(payload.getPayload() != null) {
				appendSectionInLine(sb, "Payload : ", payload.getPayload(), true);
			}
			sb.append("<b>Request : </b><br/>\n");
			appendSectionInLine(sb, "Method : ", payload.getRequest().getMethod(),false);
			if(payload.getRequest().getHeaders()!= null ) {
				sb.append(formatHeaders(payload.getRequest().getHeaders()));
			}
			sb.append("<b>Response : </b><br/>\n");
			sb.append(this.getCodeAsHtml(payload.getResponse().getDecodedContents()));
		}
		return sb.toString();
	}

	private String formatHeaders(Header[] headers) {
		StringBuilder sb = new StringBuilder();
		sb.append("Headers :<br/>\n");
		for (Header header : headers ) {
			appendSectionInLine(sb,header.getKey(), header.getValue(),false);
		}
		return sb.toString();
	}

	private void appendSectionInLine(StringBuilder sb, String header, String text, boolean bold) {

		if ( StringUtils.isNotBlank(text)) {
			if(bold) {
				sb.append("<b>").append(header).append("</b>").append(text).append("<br/>\n");
			}else{
				sb.append(header).append(text).append("<br/>\n");
			}
		}
	}

	private String getPath(String urlString){
		URL url = null;
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if (url != null) {
			return url.getFile();
		}
		return null;
	}

	private WASC getWasc(QID qid, ScanReport scanReport) {
		WASC[] wascs = scanReport.getGlossary().getWascList();
		for (WASC wasc:wascs ) {
			if(wasc.getCode().equals(qid.getWasc())) return wasc;
		}
		return null;
	}

	private OWASP getOwasp(QID qid, ScanReport scanReport) {
		OWASP[] owasps = scanReport.getGlossary().getOwaspList();
		for (OWASP owasp: owasps ) {
			if(owasp.getCode().equals(qid.getOwasp())) return owasp;
		}
		return null;
	}

	private Group getGroup(QID qid, ScanReport scanReport) {
		Group[] groups = scanReport.getGlossary().getGroups();
		for (Group group: groups ) {
			if(group.getCode().equals(qid.getGroup())) return group;
		}
		return null;
	}

	private SeverityLevel getSeverityLevel(QID qid, SeverityCategory severityCategory) {
		SeverityLevel[] severityLevels = severityCategory.getSeverityLevels();
		for (SeverityLevel severityLevel: severityLevels ) {
			if(severityLevel.getSeverity().equals(qid.getSeverity())) return severityLevel;
		}
		return null;
	}

	private SeverityCategory getSeverityCategory(QID qid, ScanReport scanReport) {
		SeverityCategory[] severityCategories = scanReport.getAppendix().getSeverityCategory();
		for (SeverityCategory severityCategory : severityCategories) {
			if(severityCategory.getName().equals(qid.getCategory())) return severityCategory;
		}
		return null;
	}

	private QID getVulnerabilityQID(Vulnerability vulnerability, ScanReport scanReport) {
		QID[] qidList = scanReport.getGlossary().getQidList();
		for (QID qid : qidList) {
			if(qid.getQid().equals(vulnerability.getQid())) return qid;
		}
		return null;
	}

	private Priority getPriority(SeverityLevel severityLevel) {
		return MAP_SEVERITY_TO_PRIORITY.getOrDefault(severityLevel.getSeverity(), Priority.Medium);
	}

	private String getCodeAsHtml(String code) {
		StringBuilder sb = new StringBuilder();
		if ( StringUtils.isNotBlank(code) ) {
			final String codePrefix = "<pre><code>";
			final String codeSuffix = "</code></pre>";

			sb.append(codePrefix)
				.append(code)
				.append(codeSuffix);
		}
		return sb.toString();
	}
}
