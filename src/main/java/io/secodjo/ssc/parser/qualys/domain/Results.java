package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

@Getter
public class Results {
    @JacksonXmlElementWrapper(localName = "VULNERABILITY_LIST")
    @JacksonXmlProperty(localName = "VULNERABILITY")
    Vulnerability[] vulnerability;
}
