package io.secodjo.ssc.parser.qualys.domain;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

@Getter
public class Appendix {
    @JacksonXmlElementWrapper(localName = "SEVERITY_CATEGORY_LIST")
    @JacksonXmlProperty(localName = "SEVERITY_CATEGORY")
    SeverityCategory[] severityCategory;

    @JacksonXmlElementWrapper(localName = "SCAN_LIST")
    @JacksonXmlProperty(localName = "SCAN")
    Scan[] scans;
}
