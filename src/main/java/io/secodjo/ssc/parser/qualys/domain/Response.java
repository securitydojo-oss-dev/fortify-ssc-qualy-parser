package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Getter
public class Response {

    @JsonProperty("CONTENTS")
    String contents;

    public String getDecodedContents(){
        return new String(Base64.getDecoder().decode(this.contents), StandardCharsets.UTF_8);
    }
}
