package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Header {
    @JsonProperty("key")
    String key;
    @JsonProperty("value")
    String value;
}
