package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class QID {
    @JsonProperty("QID")
    String qid;
    @JsonProperty("CATEGORY")
    String category;
    @JsonProperty("SEVERITY")
    String severity;
    @JsonProperty("TITLE")
    String title;
    @JsonProperty("GROUP")
    String group;
    @JsonProperty("OWASP")
    String owasp;
    @JsonProperty("WASC")
    String wasc;
    @JsonProperty("CWE")
    String cwe;
    @JsonProperty("CVSS_BASE")
    String cvssBase;
    @JsonProperty("CVSS_TEMPORAL")
    String cvssTemporal;
    @JsonProperty("DESCRIPTION")
    String description;
    @JsonProperty("IMPACT")
    String impact;
    @JsonProperty("SOLUTION")
    String solution;

}
