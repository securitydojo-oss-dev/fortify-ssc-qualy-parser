package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class SeverityLevel {
    @JsonProperty("SEVERITY")
    String severity;
    @JsonProperty("LEVEL")
    String level;
    @JsonProperty("DESCRIPTION")
    String description;
}
