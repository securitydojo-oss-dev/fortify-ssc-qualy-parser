package io.secodjo.ssc.parser.qualys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Scan {
    @JsonProperty("NAME")
    String name;
    @JsonProperty("REFERENCE")
    String reference;
    @JsonProperty("START_DATE")
    String startDate;
    @JsonProperty("END_DATE")
    String endDate;
    @JsonProperty("MODE")
    String mode;
    @JsonProperty("TYPE")
    String type;
    @JsonProperty("WEB_APPLICATION")
    String webApplication;
    @JsonProperty("PROFILE")
    String profile;


}
